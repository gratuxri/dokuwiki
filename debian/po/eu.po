# translation of dokuwiki_0.0.20101107a-2_eu.po to Basque
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Piarres Beobide <pi@beobide.net>, 2008.
# Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: dokuwiki_0.0.20101107a-2_eu\n"
"Report-Msgid-Bugs-To: dokuwiki@packages.debian.org\n"
"POT-Creation-Date: 2013-10-27 19:00+0100\n"
"PO-Revision-Date: 2011-05-08 22:57+0200\n"
"Last-Translator: Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>\n"
"Language-Team: Basque <debian-l10n-basque@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"\n"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "apache2"
msgstr "apache2"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "lighttpd"
msgstr "lighttpd"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid "Web server(s) to configure automatically:"
msgstr "Web-zerbitzariak automatikoki konfiguratzeko:"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"DokuWiki runs on any web server supporting PHP, but only listed web servers "
"can be configured automatically."
msgstr ""
"DokuWiki-k PHP euskarria duen edozer web-zerbitzari mota onartzen du, baina "
"zerrendatutako web-zerbitzariak soilik konfigura daiteke automatikoki."

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"Please select the web server(s) that should be configured automatically for "
"DokuWiki."
msgstr ""
"Hautatu web-zerbitzariak DokuWiki-rentzako automatikoki konfiguratzeko."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Should the web server(s) be restarted now?"
msgstr "Web-zerbitzariak orain berrabiarazi behar dira?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"In order to activate the new configuration, the reconfigured web server(s) "
"have to be restarted."
msgstr ""
"Konfigurazio berria aktibatzeko, birkonfiguratutako web-zerbitzariak "
"berrabiarazi egin behar dira."

#. Type: string
#. Description
#: ../templates:3001
msgid "Wiki location:"
msgstr "Wiki kokalekua:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Specify the directory below the server's document root from which DokuWiki "
"should be accessible."
msgstr ""
"Zerbitzariko dokumentuen erroaren pean dagoen direktorioa zehaztu, DokuWiki-"
"katzitu ahal dezakena."

#. Type: select
#. Description
#: ../templates:4001
msgid "Authorized network:"
msgstr "Sare baimendua:"

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"Wikis normally provide open access to their content, allowing anyone to "
"modify it. Alternatively, access can be restricted by IP address."
msgstr ""
"Wiki-ek normaleak beren edukietara sarbide irekia hornitzen dute, edonork "
"aldatzea baimenduz. Horren ordez, sarbidetza IP helbidearen bidez murriz "
"daiteke."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"If you select \"localhost only\", only people on the local host (the machine "
"the wiki is running on) will be able to connect. \"local network\" will "
"allow people on machines in a local network (which you will need to specify) "
"to talk to the wiki. \"global\" will allow anyone, anywhere, to connect to "
"the wiki."
msgstr ""
"'ostalari-lokala bakarrik' hautatzen baduzu, makina lokaleko (wikia "
"abiarazirik duen makina) erabiltzaileak bakarrik konektatu ahal izango dira. "
"'sare lokala' erabiliz, sare lokaleko beste makinei sarrera baimenduko zaie "
"(sare lokala ezarri beharko duzu). 'orokorra' erabiliz, sarrera edonori "
"edonondik baimenduko zaio."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"The default is for site security, but more permissive settings should be "
"safe unless you have a particular need for privacy."
msgstr ""
"Lehenestia gunearen segurtasunerako izan arren, baimen gehiago eskaintzen "
"duen ezarpen bat ere segurua izan daiteke (pribatutasunaren behar berezi bat "
"ez baduzu bederen)."

#. Type: select
#. Choices
#: ../templates:4002
msgid "localhost only"
msgstr "ostalari-lokala bakarrik"

#. Type: select
#. Choices
#: ../templates:4002
msgid "local network"
msgstr "sare lokala"

#. Type: select
#. Choices
#: ../templates:4002
msgid "global"
msgstr "orokorra"

#. Type: string
#. Description
#: ../templates:5001
msgid "Local network:"
msgstr "Sare lokala:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"The specification of your local network should either be an IP network in "
"CIDR format (x.x.x.x/y) or a domain specification (like .example.com)."
msgstr ""
"Sare lokalaren ezarpena CIDR formatuko (x.x.x.x/y) IP helbide bat edo "
"domeinu bat (adibidez .niredomeinua.com) zehaztuz egin behar da."

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Anyone who matches this specification will be given full and complete access "
"to DokuWiki's content."
msgstr ""
"Zehaztapenarekin bat datorren edozeini emango zaio erabateko sarbidetza "
"DokuWiki-ra."

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Purge pages on package removal?"
msgstr "Orriak ezabatu paketea kentzean?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"By default, DokuWiki stores all its pages in a file database in /var/lib/"
"dokuwiki."
msgstr ""
"Lehenespenez Dokuwiki-k bere orrialde guztiak datu-base batean gordetzen "
"ditu (/var/lib/dokuwiki direktorioan)."

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Accepting this option will leave you with a tidier system when the DokuWiki "
"package is removed, but may cause information loss if you have an "
"operational wiki that gets removed."
msgstr ""
"Aukera hau onartzean sistema garbiagoa geldituko da DokuWiki paketea "
"kentzean, baina datu galera sor dezake exekutatzen ari den wiki bat kentzen "
"bada."

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Make the configuration web-writeable?"
msgstr "Konfigurazioa web bidez idazgarria egin?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"DokuWiki includes a web-based configuration interface. To be usable, it "
"requires the web server to have write permission to the configuration "
"directory."
msgstr ""
"DokuWiki-ak web bidez konfiguratzeko interfazea dauka. Hau erabiltzeko, web-"
"zerbitzariak konfigurazioko direktorioan idazteko baimena behar du."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"Accepting this option will give the web server write permissions on the "
"configuration directory and files."
msgstr ""
"Aukera hau onartzean web-zerbitzariari konfigurazioko direktorio eta "
"fitxategietan idazteko baimenak emango zaizkio."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"The configuration files will still be readable and editable by hand "
"regardless of whether or not you accept this option."
msgstr ""
"Konfigurazioko fitxategiak eskuz irakur eta edita daitezke, berdin izanik "
"aukera hau onartzen duzun edo ez."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Make the plugins directory web-writeable?"
msgstr "Pluginen direktorioa web bidez idazgarria egin?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"DokuWiki includes a web-based plugin installation interface. To be usable, "
"it requires the web server to have write permission to the plugins directory."
msgstr ""
"DokuWiki-ak web bidez pluginak instalatzeko interfazea dauka. Hau "
"erabiltzeko, web-zerbitzariak pluginen direktorioan idazteko baimena behar "
"du."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Accepting this option will give the web server write permissions to the "
"plugins directory."
msgstr ""
"Aukera hau onartzean web-zerbitzariari pluginen direktorioan idazteko "
"baimenak emango zaizkio."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Plugins can still be installed by hand regardless of whether or not you "
"accept this option."
msgstr ""
"Pluginak eskuz instala daitezke, berdin izanik aukera hau onartzen duzun edo "
"ez."

#. Type: string
#. Description
#: ../templates:9001
msgid "Wiki title:"
msgstr "Wiki-aren titulua:"

#. Type: string
#. Description
#: ../templates:9001
msgid ""
"The wiki title will be displayed in the upper right corner of the default "
"template and on the browser window title."
msgstr ""
"Txantiloi lehenetsiaren goi-eskuineko ertzean eta arakatzailearen leihoko "
"tituluan bistaratuko den Wiki-aren titulua."

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC0 \"No Rights Reserved\""
msgstr "CC0 \"Eskubideak Gorde Gabe\""

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution"
msgstr "CC Aitortu"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-ShareAlike"
msgstr "CC Aitortu-PartekatuBerdin"

#. Type: select
#. Choices
#: ../templates:10001
msgid "GNU Free Documentation Licence"
msgstr "GNUren Dokumentazio Librearen Lizentzia"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial"
msgstr "CC Aitortu-EzKomertziala"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial-ShareAlike"
msgstr "CC Aitortu-Ezkomertziala-PartekatuBerdin"

#. Type: select
#. Description
#: ../templates:10002
msgid "Wiki license:"
msgstr "Wiki-aren lizentzia:"

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Please choose the license you want to apply to your wiki content. If none of "
"these licenses suits your needs, you will be able to add your own to the "
"file /etc/dokuwiki/license.php and to refer it in the main configuration "
"file /etc/dokuwiki/local.php when the installation is finished."
msgstr ""

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Creative Commons \"No Rights Reserved\" is designed to waive as many rights "
"as legally possible."
msgstr ""
"Creative Commons-eko \"Eskubideak Gorde Gabe\" eskubide gehienak baimentzeko "
"diseinatuta dago (legeak uzten duen heinean)."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution is a permissive license that only requires licensees to give "
"credit to the author."
msgstr ""
"CC Aitortu lizentziak egilearen kredituaren aitorpena egitea soilik eskatzen "
"du."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-ShareAlike and GNU Free Documentation License are copyleft-"
"based free licenses (requiring modifications to be released under similar "
"terms)."
msgstr ""
"CC Aitortu-PartekatuBerdin eta GNUren Dokumentazio Librearen Lizentziak "
"'copyleft' kontzeptuan oinarritutako lizentziak dira (egiten diren aldaketak "
"antzeko lizentzien pean argitaratzera derrigortuz)."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-NonCommercial and CC Attribution-Noncommercial-ShareAlike are "
"non-free licenses, in that they forbid commercial use."
msgstr ""
"CC Aitortu-EzKomertziala eta CC Aitortu-EzKomertziala-PartekatuBerdin "
"lizentzia ez-libreak dira, haien erabilpen komertziala debekatzen baitute."

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Enable ACL?"
msgstr "Gaitu ACLa?"

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"Enable this to use an Access Control List for restricting what the users of "
"your wiki may do."
msgstr ""
"Gaitu hau Sarbideko Kontrol Zerrenda (Access Control List edo ACL) bat "
"erabiltzeko zure wiki-ko erabiltzaileek zer egin dezaketen mugatzeko."

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"This is a recommended setting because without ACL support you will not have "
"access to the administration features of DokuWiki."
msgstr ""
"Hau gomendagarria da ACL euskarririk gabe ez baituzu DokuWiki-a "
"administratzeko sarbidetzarik edukiko."

#. Type: string
#. Description
#: ../templates:12001
msgid "Administrator username:"
msgstr "Administratzailearen erabiltzaile-izena:"

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"Please enter a name for the administrator account, which will be able to "
"manage DokuWiki's configuration and create new wiki users. The username "
"should be composed of lowercase ASCII letters only."
msgstr ""
"DokuWiki-a konfiguratu eta wiki erabiltzaile berriak kudeatzeaz arduratuko "
"den administratzailearen kontuarentzako izen bat sartu. erabiltzaile-izenak "
"letra minuskulaz osatuta egon behar du ASCII kodeketan."

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"If this field is left blank, no administrator account will be created now."
msgstr ""
"Eremu hau hutsik uzten bada, ez da administratzailearen konturik sortuko "
"orain."

#. Type: string
#. Description
#: ../templates:13001
msgid "Administrator real name:"
msgstr "Administratzailearen benetako izena:"

#. Type: string
#. Description
#: ../templates:13001
msgid ""
"Please enter the full name associated with the wiki administrator account. "
"This name will be stored in the wiki password file as an informative field, "
"and will be displayed with the wiki page changes made by the administrator "
"account."
msgstr ""
"Sartu wiki-ko administratzailearen izen osoa. Izen hau wiki-aren pasahitzen "
"fitxategian gordeko da informazioko eremu gisa, eta administratzailearen "
"kontuak aldatutako wiki-ko orrialdeekin bistaratuko da."

#. Type: string
#. Description
#: ../templates:14001
msgid "Administrator email address:"
msgstr "Administratzailearen helb. elektronikoa:"

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"Please enter the email address associated with the wiki administrator "
"account. This address will be stored in the wiki password file, and may be "
"used to get a new administrator password if you lose the original."
msgstr ""
"Sartu wiki-ko administratzailearen helbide elektronikoa, Izen hau wiki-aren "
"pasahitzen fitxategian gordeko da, eta administratzailearen jatorrizko "
"pasahitza galtzen denean berri bat lortzeko erabiliko da."

#. Type: password
#. Description
#: ../templates:15001
msgid "Administrator password:"
msgstr "Administratzailearen pasahitza:"

#. Type: password
#. Description
#: ../templates:15001
msgid "Please choose a password for the wiki administrator."
msgstr "Aukeratu pasahitz bat wiki-ko administratzailearentzako."

#. Type: password
#. Description
#: ../templates:16001
msgid "Re-enter password to verify:"
msgstr "Berretsi pasahitza:"

#. Type: password
#. Description
#: ../templates:16001
msgid ""
"Please enter the same \"admin\" password again to verify you have typed it "
"correctly."
msgstr ""
"Sartu berriro administratzailearen pasahitza ongi idatzi duzula egiaztatzeko."

#. Type: error
#. Description
#: ../templates:17001
msgid "Password input error"
msgstr "Errorea pasahitzan"

#. Type: error
#. Description
#: ../templates:17001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Sartutako pasahitzak ez dira berdina. Saiatu berriro."

#. Type: select
#. Description
#: ../templates:18001
msgid "Initial ACL policy:"
msgstr "Hasierako ACL arauak:"

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"Please select what initial ACL configuration should be set up to match the "
"intended usage of this wiki:\n"
" \"open\":   both readable and writeable for anonymous users;\n"
" \"public\": readable for anonymous users, writeable for registered users;\n"
" \"closed\": readable and writeable for registered users only."
msgstr ""
"Hautau ACLaren hasierako konfigurazioa wiki honen erabilerarekin bat "
"etortzeko:\n"
" \"irekia\": erabiltzaile anonimoek bai irakurri bai idatz dezakete;\n"
" \"publikoa\": erabiltzaile anonimoek soilik irakur dezakete; "
"harpidetutakoek baita idatzi ere;\n"
" \"itxia\": harpidetutako erabiltzaileek soilik irakur eta idatz dezakete."

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"This is only an initial setup; you will be able to adjust the ACL rules "
"later."
msgstr "Hau ashierako konfigurazioa da; beranduago doi ditzakezu ACL arauak."

#. Type: select
#. Choices
#: ../templates:18002
msgid "open"
msgstr "irekia"

#. Type: select
#. Choices
#: ../templates:18002
msgid "public"
msgstr "publikoa"

#. Type: select
#. Choices
#: ../templates:18002
msgid "closed"
msgstr "itxia"

#~ msgid "Please choose the license you want to apply to your wiki content."
#~ msgstr "Aukeratu wiki-aren edukiari aplikatzea nahi diozun lizentzia."
